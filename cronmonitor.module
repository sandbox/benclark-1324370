<?php

define('CRONMONITOR_DEFAULT_WARNING_THRESHOLD', 43200); // 12 hours

/**
 * Implements hook_cron().
 */
function cronmonitor_cron() {
  if (!cronmonitor_is_server()) {
    module_load_include('inc', 'cronmonitor');
    _cronmonitor_client_cron();
  }
}

/**
 * Implements hook_menu().
 */
function cronmonitor_menu() {
  $items = array();

  $items['cronmonitor-ping'] = array(
    'title' => 'Cron Monitor',
    'type' => MENU_CALLBACK,
    'page callback' => 'cronmonitor_ping',
    'page arguments' => array(1),
    'access callback' => 'cronmonitor_is_server',
    'file' => 'cronmonitor.inc',
  );
  $items['admin/reports/cronmonitor'] = array(
    'title' => 'Cron Monitor Report',
    'description' => 'Aggregated report of all cron monitoring.',
    'page callback' => 'cronmonitor_admin_report',
    'access callback' => 'cronmonitor_report_access',
    'file' => 'cronmonitor.admin.inc',
  );
  $items['admin/settings/cronmonitor'] = array(
    'title' => 'Cron Monitor',
    'description' => 'Track cron runs from other Drupal sites.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cronmonitor_admin_settings'),
    'access arguments' => array('administer cronmonitor'),
    'file' => 'cronmonitor.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_perm().
 */
function cronmonitor_perm() {
  return array('administer cronmonitor', 'view cronmonitor report');
}

/**
 * Access callback for the report view.
 */
function cronmonitor_report_access() {
  return cronmonitor_is_server() && user_access('view cronmonitor report');
}

/**
 * Returns TRUE if this instance is configured to be a server.
 */
function cronmonitor_is_server() {
  return variable_get('cronmonitor_mode', 'client') == 'server';
}
