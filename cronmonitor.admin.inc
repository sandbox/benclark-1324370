<?php

/**
 * List of all clients that have pinged this server.
 */
function cronmonitor_admin_report() {
  $header = array(
    t('Client'),
    t('Status'),
    t('Last updated'),
  );
  $rows = array();

  $threshold = variable_get('cronmonitor_warning_threshold', CRONMONITOR_DEFAULT_WARNING_THRESHOLD);

  $result = db_query('SELECT site, MAX(`timestamp`) AS `timestamp` FROM {cronmonitor} GROUP BY site');
  while ($db_row = db_fetch_object($result)) {
    $time_ago = time() - $db_row->timestamp;
    if ($time_ago >= $threshold) {
      $class = 'error';
      $status = t('Failed');
    }
    elseif ($time_ago >= $threshold * 0.5) {
      $class = 'warning';
      $status = t('Warning');
    }
    else {
      $class = '';
      $status = '';
    }
    $rows[] = array(
      'data' => array(
        $db_row->site,
        $status,
        format_interval($time_ago),
      ),
      'class' => $class,
    );
  }

  return theme('table', $header, $rows);
}

/**
 * Configure settings.
 */
function cronmonitor_admin_settings() {
  $form = array();

  $form['cronmonitor_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Client/server mode for this Drupal instance'),
    '#description' => t('If this Drupal instance will be monitoring cron runs from other sites, select Server. If this Drupal instance will be sending out data to another location, select Client.'),
    '#options' => array(
      'client' => t('Client'),
      'server' => t('Server'),
    ),
    '#default_value' => variable_get('cronmonitor_mode', 'client'),
  );

  $form['client'] = array(
    '#type' => 'fieldset',
    '#title' => t('Client configuration'),
    '#collapsible' => TRUE,
  );
  $form['client']['cronmonitor_ping_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Send cron report to this URL'),
    '#description' => t('This is the URL provided to you by the settings form on the server.'),
    '#default_value' => variable_get('cronmonitor_ping_url', NULL),
  );

  $form['server'] = array(
    '#type' => 'fieldset',
    '#title' => t('Server configuration'),
    '#collapsible' => TRUE,
  );
  $form['server']['cronmonitor_server_url'] = array(
    '#type' => 'item',
    '#title' => t('Configure clients to send to this URL'),
    '#description' => t('Provide this URL in the settings form on each client. Optionally specify a client ID, otherwise will use referrer data from the ping request.'),
    '#value' => url('cronmonitor-ping', array('absolute' => TRUE)) . '/%client_id',
  );
  $form['server']['cronmonitor_warning_threshold'] = array(
    '#type' => 'select',
    '#title' => t('Threshold for cron failure warnings'),
    '#description' => t('If a client hasn\'t pinged within this threshold, mark this client as failed.'),
    '#options' => array(
      '1800' => t('30 minutes'),
      '3600' => t('1 hour'),
      '7200' => t('2 hours'),
      '21600' => t('6 hours'),
      '43200' => t('12 hours'),
      '86400' => t('1 day'),
      '604800' => t('1 week'),
    ),
    '#default_value' => variable_get('cronmonitor_warning_threshold', CRONMONITOR_DEFAULT_WARNING_THRESHOLD),
  );

  return system_settings_form($form);
}

/**
 * Validate the settings form.
 */
function cronmonitor_admin_settings_validate($form, $form_state) {
  $values = &$form_state['values'];
  if (!empty($values['cronmonitor_ping_url']) && !valid_url($values['cronmonitor_ping_url'])) {
    form_set_error('cronmonitor_ping_url', t('This field needs to be a valid URL.'));
  }
}
