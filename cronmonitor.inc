<?php

/**
 * Cron callback for Cron Monitor clients.
 */
function _cronmonitor_client_cron() {
  // Send out the ping.
  if ($ping_url = variable_get('cronmonitor_ping_url', NULL)) {
    if (valid_url($ping_url)) {
      // The response is irrelevant, it's all about the request.
      $retval = drupal_http_request($ping_url);
    }
  }
}

/**
 * Menu callback for receiving client pings.
 */
function cronmonitor_ping($client = NULL) {
  if (empty($client)) {
    // Grab the remote IP and use that as client.
    $client = $_SERVER['REMOTE_ADDR'];
  }
  if (empty($client)) {
    // If client is still empty, don't insert a db record.
    return;
  }

  // Insert a database record for this ping.
  $sql = "INSERT INTO {cronmonitor} (site, `timestamp`) VALUES ('%s', %d)";
  db_query($sql, $client, time());

  // Do some db cleanup here, drop old entries, that sort of thing.
}
